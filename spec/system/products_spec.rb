require 'rails_helper'

RSpec.describe "商品詳細ページのテスト", type: :system do
  describe "GET #show" do
    let(:product) { create(:product, taxons: [taxon]) }
    let(:taxon)   { create(:taxon, taxonomy: taxonomy) }
    let(:taxonomy) { create(:taxonomy) }
    let!(:related_product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      visit potepan_product_path(product.id)
    end

    it "商品名が正しく表示される" do
      expect(page).to have_content product.name
    end

    it "商品価格が正しく表示される" do
      expect(page).to have_content product.price
    end

    it "商品説明が正しく表示される" do
      expect(page).to have_content product.description
    end

    it "商品画像が正しく表示される" do
      product.images.each_with_index do |image, index|
        expect(response.body).to include product.image.attachment(:small)
        expect(response.body).to include product.image.attachment(:large)
      end
    end

    it "「一覧ページへ戻る」をクリックすると正しく遷移する" do
      click_link '一覧ページへ戻る'
      expect(current_path).to eq potepan_category_path(taxon.id)
    end

    it "関連商品をクリックすると関連商品詳細ページに遷移する" do
      expect(page).to have_content related_product.name
      click_link related_product.name
      expect(current_path).to eq potepan_product_path(related_product.id)
    end

    it "関連商品が４つ表示される" do
      expect(page).to have_selector ".productBox", count: 4
    end
  end
end
