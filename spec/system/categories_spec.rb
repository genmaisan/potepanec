require 'rails_helper'

RSpec.describe "商品カテゴリーページ", type: :system do
  describe "GET #show" do
    let!(:product) { create(:product, taxons: [taxon]) }
    let(:taxon)    { create(:taxon, taxonomy: taxonomy) }
    let(:taxonomy) { create(:taxonomy) }
    let!(:bag)     { taxonomy.root.children.create(name: "bag") }

    before do
      visit potepan_category_path(taxon.id)
    end

    it "page_headerに商品名,Homeが表示されている" do
      expect(page).to have_selector "h2", text: taxon.name
      expect(page).to have_link "Home"
      expect(page).to have_link "Shop"
      expect(page).to have_selector "li.active", text: taxon.name
    end

    it "サイドバーに商品が表示される" do
      expect(page).to have_content taxon.name
      expect(page).to have_content taxonomy.name
      expect(page).to have_content bag.name
    end

    it "商品カテゴリをクリックすると、個別のカテゴリページに遷移する" do
      click_link bag.name
      expect(current_path).to eq potepan_category_path(bag.id)
    end

    it "色から探すリストに各色が表示されている" do
      expect(page).to have_link "ブラック"
      expect(page).not_to have_link "グリーン"
    end

    it "サイズから探すリストに各サイズが表示されている" do
      expect(page).to have_link "S"
      expect(page).not_to have_link "A"
    end

    it "商品名が表示されている" do
      expect(page).to have_link product.name
      expect(page).to have_link product.display_price
    end

    it "商品名をクリックすると、商品詳細ページに遷移する" do
      expect(page).to have_content product.name
      click_link product.name
      expect(current_path).to eq potepan_product_path(product.id)
    end
  end
end
