require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "GET #show" do
    let(:product) { create(:product, taxons: [taxon]) }
    let(:taxon)   { create(:taxon, taxonomy: taxonomy) }
    let(:taxonomy) { create(:taxonomy) }

    before do
      get potepan_category_path taxon.id
    end

    it "カテゴリーページが開く" do
      expect(response.status).to eq 200
    end

    it 'taxon名を取得している' do
      expect(response.body).to include taxon.name
    end

    it 'taxonomy名を取得している' do
      expect(response.body).to include taxonomy.name
    end
  end
end
