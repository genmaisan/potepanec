require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  describe "GET #show" do
    let(:product) { create(:product, taxons: [taxon]) }
    let(:taxon)    { create(:taxon, taxonomy: taxonomy) }
    let(:taxonomy) { create(:taxonomy) }
    let!(:related_product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_product_path product.id
    end

    it "商品詳細ページを開く" do
      expect(response.status).to eq 200
    end

    it "product名を取得できている" do
      expect(response.body).to include product.name
    end

    it "productの値段を取得できている" do
      expect(response.body).to include product.display_price.to_s
    end

    it "productの説明を取得できている" do
      expect(response.body).to include product.description
    end

    it "関連商品名が取得できている" do
      expect(response.body).to include related_product.name
    end

    it "関連商品の値段が取得できている" do
      expect(response.body).to include related_product.display_price.to_s
    end
  end
end
