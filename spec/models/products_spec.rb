require 'rails_helper'

RSpec.describe "関連商品モデルのテスト", type: :model do
  describe "related_productsメソッド" do
    let!(:taxon1)   { create(:taxon) }
    let!(:taxon2)   { create(:taxon) }
    let!(:product)  { create(:product, taxons: [taxon1]) }
    let!(:products) { create_list(:product, 4, taxons: [taxon1]) }
    let!(:product5) { create(:product, taxons: [taxon2]) }

    it "関連する商品のみを取得し、関連しない商品は含まれない" do
      expect(product.related_products).to match_array(products)
    end
  end
end
